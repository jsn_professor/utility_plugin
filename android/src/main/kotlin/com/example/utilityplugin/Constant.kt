package com.example.utilityplugin

/**
 * Created by Professor on 12/11/15.
 */
class Constant {
    companion object {
        const val BASE_URI_MARKET = "market://details?id="
        const val BASE_URI_PLAY_STORE = "https://play.google.com/store/apps/details?id="
    }

    object RequestCode {
        const val PERMISSION = 0
        const val QR_CODE = 1
        const val SIGN_IN_WITH_APPLE = 2
    }

    object Key {
        const val PERMISSIONS: String = BuildConfig.LIBRARY_PACKAGE_NAME + ".PERMISSIONS"
        const val URL: String = BuildConfig.LIBRARY_PACKAGE_NAME + ".URL"
        const val CODE: String = BuildConfig.LIBRARY_PACKAGE_NAME + ".CODE"
        const val ID_TOKEN: String = BuildConfig.LIBRARY_PACKAGE_NAME + ".ID_TOKEN"
    }
}
