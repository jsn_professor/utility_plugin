package com.example.utilityplugin

import android.app.DownloadManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.example.utilityplugin.activity.ScannerActivity
import com.example.utilityplugin.activity.SignInWithAppleActivity
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import java.io.File
import kotlin.random.Random

/** UtilityPlugin */
class UtilityPlugin : FlutterPlugin, ActivityAware, PluginRegistry.ActivityResultListener,
    PluginRegistry.NewIntentListener, MethodCallHandler {
    private var binding: ActivityPluginBinding? = null
    private var methodChannel: MethodChannel? = null
    private var pendingResult: Result? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel = MethodChannel(
            flutterPluginBinding.binaryMessenger,
            "utility_plugin_method_channel"
        ).apply {
            setMethodCallHandler(this@UtilityPlugin)
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel?.setMethodCallHandler(null)
        methodChannel = null
        pendingResult = null
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "downloadUpdate" -> {
                val activity = binding?.activity ?: return result.success(false)
                val url = call.argument<String>("url") ?: return result.success(false)
                val title = call.argument<String>("title")
                val uri = Uri.parse(url)
                val oldFile = File(
                    activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                    uri.lastPathSegment
                )
                if (oldFile.exists()) {
                    oldFile.delete()
                }
                val downloadManager =
                    activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                val query = DownloadManager.Query().apply {
                    setFilterByStatus(DownloadManager.STATUS_PAUSED or DownloadManager.STATUS_PENDING or DownloadManager.STATUS_RUNNING)
                }
                downloadManager.query(query).use {
                    while (it.moveToNext()) {
                        if (uri.toString() == it.getString(it.getColumnIndex(DownloadManager.COLUMN_URI))) {
                            return result.success(false)
                        }
                    }
                }
                val request = DownloadManager.Request(uri).apply {
                    setTitle(title ?: uri.lastPathSegment)
                    setDestinationInExternalFilesDir(
                        activity,
                        Environment.DIRECTORY_DOWNLOADS,
                        uri.lastPathSegment
                    )
                }
                val downloadId = downloadManager.enqueue(request)
                activity.registerReceiver(object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        if (downloadId != intent.getLongExtra(
                                DownloadManager.EXTRA_DOWNLOAD_ID,
                                -1
                            )
                        ) {
                            return
                        }
                        val query = DownloadManager.Query().apply {
                            setFilterById(downloadId)
                        }
                        downloadManager.query(query).use {
                            if (it.moveToFirst() && it.getInt(it.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                val install = Intent(Intent.ACTION_VIEW).apply {
                                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                }
                                val localUri =
                                    it.getString(it.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                                val mediaType =
                                    it.getString(it.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE))
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    val uri = FileProvider.getUriForFile(
                                        context,
                                        context.applicationContext.packageName + ".provider",
                                        File(Uri.parse(localUri).path)
                                    )
                                    install.setDataAndType(uri, mediaType)
                                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                } else {
                                    install.setDataAndType(Uri.parse(localUri), mediaType)
                                }
                                handleIntent(context, install)
                            }
                        }
                        context.unregisterReceiver(this)
                    }
                }, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
                result.success(true)
            }
            "openBrowser" -> {
                val activity = binding?.activity ?: return result.success(false)
                val url = call.argument<String>("url") ?: return result.success(false)
                try {
                    val customTabsIntent = CustomTabsIntent.Builder().apply {
                        call.argument<Long>("color")?.toInt()?.let { setToolbarColor(it) }
                    }.build()
                    customTabsIntent.launchUrl(activity, Uri.parse(url))
                    result.success(true)
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                    result.success(false)
                }
            }
            "openSettings" -> {
                val activity = binding?.activity ?: return result.success(false)
                activity.startActivity(Intent(Settings.ACTION_SETTINGS))
                result.success(true)
            }
            "showToast" -> {
                val activity = binding?.activity ?: return result.success(false)
                Toast.makeText(activity, call.arguments?.toString(), Toast.LENGTH_LONG).apply {
                    view?.background =
                        ContextCompat.getDrawable(activity, R.drawable.shape_toast_background)
                    view?.findViewById<TextView>(android.R.id.message)
                        ?.setTextColor(activity.resources.getColor(android.R.color.white))
                }.show()
                result.success(true)
            }
            "getMetaData" -> {
                val activity = binding?.activity ?: return result.success(null)
                val applicationInfo = activity.packageManager?.getApplicationInfo(
                    activity.packageName,
                    PackageManager.GET_META_DATA
                )
                result.success(applicationInfo?.metaData?.get(call.arguments?.toString()))
            }
            "showNotification" -> {
                val activity = binding?.activity ?: return result.success(false)
                val title = call.argument<String>("title") ?: return result.success(false)
                val text = call.argument<String>("text") ?: return result.success(false)
                val sound = call.argument<Boolean>("sound") ?: return result.success(false)
                val vibrate = call.argument<Boolean>("vibrate") ?: return result.success(false)
                val notificationChannel =
                    call.argument<String>("notificationChannel") ?: activity.packageName
                val manager = NotificationManagerCompat.from(activity)
                val builder = NotificationCompat.Builder(activity, notificationChannel).apply {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        var channel =
                            manager.notificationChannels.firstOrNull { it.name == notificationChannel }
                        if (channel == null) {
                            channel = NotificationChannel(
                                notificationChannel,
                                notificationChannel,
                                NotificationManager.IMPORTANCE_DEFAULT
                            )
                            channel.enableVibration(true)
                            manager.createNotificationChannel(channel)
                        }
                        setChannelId(channel.id)
                    }
                    setAutoCancel(true)
                    setTicker(text)
                    setSmallIcon(
                        activity.resources.getIdentifier(
                            "ic_launcher",
                            "mipmap",
                            activity.packageName
                        )
                    )
                    setContentTitle(title)
                    setContentText(text)
                    setStyle(NotificationCompat.BigTextStyle())
                    if (sound) {
                        setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    }
                    if (vibrate) {
                        setVibrate(longArrayOf(0, 250, 250, 250))
                    }
                    val intent =
                        activity.packageManager.getLaunchIntentForPackage(activity.packageName)
                    val pendingIntent = PendingIntent.getActivity(
                        activity,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                    )
                    setContentIntent(pendingIntent)
                }
                manager.notify(Random.Default.nextInt(), builder.build())
                result.success(true)
            }
            "scanQrCode" -> {
                val activity = binding?.activity ?: return result.success(null)
                pendingResult?.success(null)
                pendingResult = result
                activity.startActivityForResult(
                    Intent(activity, ScannerActivity::class.java),
                    Constant.RequestCode.QR_CODE
                )
            }
            "disableIdleTimer" -> {
                val activity = binding?.activity ?: return result.success(false)
                activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                result.success(true)
            }
            "enableIdleTimer" -> {
                val activity = binding?.activity ?: return result.success(false)
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                result.success(true)
            }
            "signInWithApple" -> {
                val activity = binding?.activity ?: return result.success(null)
                val clientId = call.argument<String>("clientId") ?: return result.success(null)
                val redirectUri =
                    call.argument<String>("redirectUri") ?: return result.success(null)
                val uri = Uri.Builder().apply {
                    scheme("https")
                    authority("appleid.apple.com")
                    path("/auth/authorize")
                    appendQueryParameter("client_id", clientId)
                    appendQueryParameter("redirect_uri", redirectUri)
                    appendQueryParameter("response_type", "code id_token")
                    call.argument<List<String>>("requestedScopes")?.let {
                        if (it.isNotEmpty()) {
                            appendQueryParameter("scope", it.joinToString(separator = " "))
                            appendQueryParameter("response_mode", "form_post")
                        }
                    }
                }.build()
                val intent = Intent(activity, SignInWithAppleActivity::class.java).apply {
                    putExtra(Constant.Key.URL, uri.toString())
                }
                if (intent.resolveActivity(activity.packageManager) != null) {
                    pendingResult?.success(null)
                    pendingResult = result
                    activity.startActivityForResult(intent, Constant.RequestCode.SIGN_IN_WITH_APPLE)
                } else {
                    result.error(
                        "intent.resolveActivity(activity.packageManager) != null",
                        null,
                        null
                    )
                }
//                val customTabsIntent = CustomTabsIntent.Builder().apply {
//                    call.argument<Long>("color")?.toInt()?.let { setToolbarColor(it) }
//                }.build()
//                if (customTabsIntent.intent.resolveActivity(activity.packageManager) != null) {
//                    pendingResult?.success(null)
//                    pendingResult = result
//                    activity.startActivity(customTabsIntent.intent.setData(uri), customTabsIntent.startAnimationBundle)
//                } else {
//                    val intent = Intent(Intent.ACTION_VIEW, uri)
//                    if (intent.resolveActivity(activity.packageManager) != null) {
//                        pendingResult?.success(null)
//                        pendingResult = result
//                        activity.startActivity(intent)
//                    } else {
//                        result.error("intent.resolveActivity(activity.packageManager) != null", null, null)
//                    }
//                }
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    private fun handleIntent(
        context: Context,
        intent: Intent,
        appPackage: String? = null
    ): Boolean {
        intent.setPackage(appPackage)
        return if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
            true
        } else {
            launchStore(context, appPackage ?: "", false)
            false
        }
    }

    private fun launchStore(context: Context, appPackage: String, clearTask: Boolean) {
        val storeIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(Constant.BASE_URI_MARKET + appPackage))
        if (clearTask) storeIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        if (storeIntent.resolveActivity(context.packageManager) != null) context.startActivity(
            storeIntent
        ) else {
            val webIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(Constant.BASE_URI_PLAY_STORE + appPackage))
            if (clearTask) webIntent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            if (webIntent.resolveActivity(context.packageManager) != null) context.startActivity(
                webIntent
            )
        }
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        binding.addActivityResultListener(this)
//        binding.addOnNewIntentListener(this)
        this.binding = binding
    }

    override fun onDetachedFromActivity() {
        binding?.removeActivityResultListener(this)
//        binding?.removeOnNewIntentListener(this)
        binding = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        onAttachedToActivity(binding)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return when (requestCode) {
            Constant.RequestCode.QR_CODE -> {
                pendingResult?.success(data?.getStringExtra(Intent.EXTRA_TEXT))
                pendingResult = null
                true
            }
            Constant.RequestCode.SIGN_IN_WITH_APPLE -> {
                val code = data?.getStringExtra(Constant.Key.CODE)
                val idToken = data?.getStringExtra(Constant.Key.ID_TOKEN)
                if (code != null && idToken != null) {
                    pendingResult?.success(
                        mapOf(
                            Pair("authorizationCode", code),
                            Pair("identityToken", idToken)
                        )
                    )
                } else {
                    pendingResult?.success(null)
                }
                pendingResult = null
                true
            }
            else -> false
        }
    }

    override fun onNewIntent(intent: Intent): Boolean {
        return if (intent.action == Intent.ACTION_VIEW && intent.data != null) {
            val code = intent.data?.getQueryParameter("code")
            val idToken = intent.data?.getQueryParameter("id_token")
            if (code != null && idToken != null) {
                pendingResult?.success(
                    mapOf(
                        Pair("authorizationCode", code),
                        Pair("identityToken", idToken)
                    )
                )
            } else {
                pendingResult?.success(null)
            }
            pendingResult = null
            true
        } else {
            false
        }
    }
}
