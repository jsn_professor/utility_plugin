package com.example.utilityplugin.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Bundle
import android.util.Size
import android.view.Surface
import android.view.SurfaceHolder
import android.view.View
import androidx.core.app.ActivityCompat
import com.example.utilityplugin.Constant
import com.example.utilityplugin.R
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import kotlinx.android.synthetic.main.activity_scanner.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class ScannerActivity : Activity(), SurfaceHolder.Callback, View.OnClickListener {
    private val manager by lazy { getSystemService(Context.CAMERA_SERVICE) as CameraManager }
    private val scanner = BarcodeScanning.getClient(BarcodeScannerOptions.Builder().setBarcodeFormats(Barcode.FORMAT_QR_CODE).build())
    private lateinit var currentCameraId: String
    private var camera: CameraDevice? = null
    private var permissions = listOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (manager.cameraIdList.isEmpty()) {
            onBackPressed()
            return
        }
        requestedOrientation = when (windowManager.defaultDisplay.rotation) {
            Surface.ROTATION_0 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            Surface.ROTATION_90 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            Surface.ROTATION_180 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
            else -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
        }
        currentCameraId = manager.cameraIdList.first()
        setContentView(R.layout.activity_scanner)
        surfaceView.holder.addCallback(this)
        imageButtonClose.setOnClickListener(this)
        imageButtonSwitch.isEnabled = false
        imageButtonSwitch.setOnClickListener(this)
        permissions = savedInstanceState?.getStringArray(Constant.Key.PERMISSIONS)?.toList()
            ?: listOf()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constant.RequestCode.PERMISSION &&
            this.permissions.size == permissions.size &&
            this.permissions.size == grantResults.size
        ) {
            var grantPermissionsCount = 0
            permissions.forEachIndexed { index, permission ->
                if (permission == permissions[index] && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    grantPermissionsCount++
                }
            }
            if (grantPermissionsCount == permissions.size) {
                openCamera()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArray(Constant.Key.PERMISSIONS, permissions.toTypedArray())
        super.onSaveInstanceState(outState)
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        val previewSize = getOptimalOutputSize(surfaceView.width, surfaceView.height, SurfaceHolder::class.java)
        surfaceView.setAspectRatio(previewSize.width, previewSize.height)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissions = listOf(Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(this, permissions.toTypedArray(), Constant.RequestCode.PERMISSION)
            return
        }
        openCamera()
    }

    override fun surfaceChanged(suface: SurfaceHolder, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(suface: SurfaceHolder) {
        camera?.close()
        this@ScannerActivity.camera = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageButtonClose -> {
                onBackPressed()
            }
            R.id.imageButtonSwitch -> {
                val index = manager.cameraIdList.indexOf(currentCameraId) + 1
                val currentCameraId = if (index < manager.cameraIdList.size) {
                    manager.cameraIdList[index]
                } else {
                    manager.cameraIdList.first()
                }
                if (this.currentCameraId != currentCameraId) {
                    imageButtonSwitch.isEnabled = false
                    this.currentCameraId = currentCameraId
                    camera?.close()
                    openCamera()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun openCamera() {
        manager.openCamera(
            currentCameraId,
            object : CameraDevice.StateCallback() {
                override fun onOpened(camera: CameraDevice) {
                    imageButtonSwitch.isEnabled = true
                    this@ScannerActivity.camera = camera
                    val imageReaderSize = getOptimalOutputSize<Void>(
                        pixelCount = 1000000,
                        aspectRatio = max(surfaceView.width, surfaceView.height).toDouble() / min(surfaceView.width, surfaceView.height).toDouble(),
                        format = ImageFormat.YUV_420_888
                    )
                    val imageReader = ImageReader.newInstance(imageReaderSize.width, imageReaderSize.height, ImageFormat.YUV_420_888, 2)
                    var processing = false
                    imageReader.setOnImageAvailableListener({
                        val image = it.acquireLatestImage()
                            ?: return@setOnImageAvailableListener
                        if (!processing) {
                            processing = true
                            scanner.process(InputImage.fromMediaImage(image, getRotationCompensation())).addOnCompleteListener(this@ScannerActivity) {
                                image.close()
                                val barcode = it.result?.singleOrNull()
                                if (barcode != null) {
                                    imageReader.close()
                                    setResult(RESULT_OK, Intent().putExtra(Intent.EXTRA_TEXT, barcode.rawValue))
                                    onBackPressed()
                                }
                                processing = false
                            }
                        } else {
                            image.close()
                        }
                    }, null)
                    camera.createCaptureSession(listOf(surfaceView.holder.surface, imageReader.surface), object : CameraCaptureSession.StateCallback() {
                        override fun onConfigured(session: CameraCaptureSession) {
                            val request = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW).apply {
                                addTarget(surfaceView.holder.surface)
                                addTarget(imageReader.surface)
                            }.build()
                            session.setRepeatingRequest(request, null, null)
                        }

                        override fun onConfigureFailed(session: CameraCaptureSession) {
                        }
                    }, null)
                }

                override fun onDisconnected(camera: CameraDevice) {
                    camera.close()
                    this@ScannerActivity.camera = null
                }

                override fun onError(camera: CameraDevice, error: Int) {
                    camera.close()
                    this@ScannerActivity.camera = null
                }

            }, null
        )
    }

    private fun getRotationCompensation(): Int {
        var rotationCompensation = when (windowManager.defaultDisplay.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            else -> 270
        }
        val characteristics = manager.getCameraCharacteristics(currentCameraId)
        val orientation = characteristics[CameraCharacteristics.SENSOR_ORIENTATION] ?: 0
        rotationCompensation = if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraMetadata.LENS_FACING_FRONT) {
            (orientation + rotationCompensation) % 360
        } else {
            (orientation - rotationCompensation + 360) % 360
        }
        return rotationCompensation
    }

    private fun <T> getOptimalOutputSize(targetWidth: Int, targetHeight: Int, targetClass: Class<T>? = null, format: Int? = null): Size {
        val streamConfigurationMap = manager.getCameraCharacteristics(currentCameraId)[CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP]
        val sizes = (if (format == null) streamConfigurationMap?.getOutputSizes(targetClass) else streamConfigurationMap?.getOutputSizes(format))
            ?: arrayOf()
        var optimalSize = Size(0, 0)
        val minTarget = min(targetWidth, targetHeight)
        val maxTarget = max(targetWidth, targetHeight)
        var absTarget = Int.MAX_VALUE
        for (size in sizes) {
            val min = min(size.width, size.height)
            val max = max(size.width, size.height)
            val abs = abs(minTarget - min) + abs(maxTarget - max)
            if (abs < absTarget) {
                absTarget = abs
                optimalSize = size
            }
        }
        return optimalSize
    }

    private fun <T> getOptimalOutputSize(pixelCount: Int, aspectRatio: Double, targetClass: Class<T>? = null, format: Int? = null): Size {
        val streamConfigurationMap = manager.getCameraCharacteristics(currentCameraId)[CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP]
        val sizes = (if (format == null) streamConfigurationMap?.getOutputSizes(targetClass) else streamConfigurationMap?.getOutputSizes(format))
            ?: arrayOf()
        val pixelCountSorted = sizes.sortedWith { lhs, rhs -> abs(lhs.width * lhs.height - pixelCount) - abs(rhs.width * rhs.height - pixelCount) }
        val aspectRatioSorted = sizes.sortedWith { lhs, rhs ->
            val result = abs(max(lhs.width, lhs.height).toDouble() / min(lhs.width, lhs.height).toDouble() - aspectRatio) -
                    abs(max(rhs.width, rhs.height).toDouble() / min(rhs.width, rhs.height).toDouble() - aspectRatio)
            when {
                result > 0 -> 1
                result < 0 -> -1
                else -> 0
            }
        }
        var optimalSize = Size(0, 0)
        var indexTarget = Int.MAX_VALUE
        for (size in sizes) {
            val index = pixelCountSorted.indexOf(size) + aspectRatioSorted.indexOf(size)
            if (index < indexTarget) {
                indexTarget = index
                optimalSize = size
            }
        }
        return optimalSize
    }
}