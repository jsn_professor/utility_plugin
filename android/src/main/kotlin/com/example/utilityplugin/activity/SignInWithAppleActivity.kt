package com.example.utilityplugin.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.utilityplugin.Constant
import com.example.utilityplugin.R

class SignInWithAppleActivity : Activity() {

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_with_apple)
        val url = intent.getStringExtra(Constant.Key.URL)
        if (url != null) {
            val uri = Uri.parse(url)
            val redirectUriString = uri.getQueryParameter("redirect_uri")
            if (redirectUriString != null) {
                val redirectUri = Uri.parse(redirectUriString)
                webView = findViewById(R.id.webView)
                webView.apply {
                    settings.javaScriptEnabled = true
                    webViewClient = object : WebViewClient() {
                        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                            return if (request?.isForMainFrame == true &&
                                request.url.authority == redirectUri.authority &&
                                request.url.path == redirectUri.path
                            ) {
                                val data = Intent().apply {
                                    putExtra(Constant.Key.CODE, request.url.getQueryParameter("code"))
                                    putExtra(Constant.Key.ID_TOKEN, request.url.getQueryParameter("id_token"))
                                }
                                setResult(RESULT_OK, data)
                                finish()
                                true
                            } else {
                                false
                            }
                        }
                    }
                }
                webView.loadUrl(url)
            } else {
                finish()
            }
        } else {
            finish()
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}