import Flutter
import UIKit
import AuthenticationServices
import iOSUtility

public class SwiftUtilityPlugin: NSObject, FlutterPlugin, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding, ScannerViewControllerDelegate {
    static var methodChannel: FlutterMethodChannel!
    private var pendingResult: FlutterResult?
    private weak var toast: Toast?

    public static func register(with registrar: FlutterPluginRegistrar) {
        let instance = SwiftUtilityPlugin()
        methodChannel = FlutterMethodChannel(name: "utility_plugin_method_channel", binaryMessenger: registrar.messenger())
        registrar.addMethodCallDelegate(instance, channel: methodChannel)
    }

    public func detachFromEngine(for registrar: FlutterPluginRegistrar) {
        SwiftUtilityPlugin.methodChannel = nil
        pendingResult = nil
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "openSettings":
            if let url = URL(string: "App-prefs:"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:])
                result(true)
            } else {
                result(false)
            }
        case "showToast":
            guard let window = UIApplication.shared.windows.last else {
                result(false)
                return
            }
            if let toast = toast {
                toast.label.setText(text: call.arguments as? String)
            } else {
                let toast = Bundle(for: self.classForCoder).loadView(from: "Toast") as! Toast
                self.toast = toast
                toast.label.text = call.arguments as? String
                window.addSubview(toast)
                let constraints: [NSLayoutConstraint]
                if #available(iOS 11.0, *) {
                    constraints = [
                        toast.centerXAnchor.constraint(equalTo: window.safeAreaLayoutGuide.centerXAnchor),
                        toast.leftAnchor.constraint(greaterThanOrEqualTo: window.safeAreaLayoutGuide.leftAnchor, constant: 40),
                        toast.rightAnchor.constraint(lessThanOrEqualTo: window.safeAreaLayoutGuide.rightAnchor, constant: -40),
                        toast.bottomAnchor.constraint(equalTo: window.safeAreaLayoutGuide.bottomAnchor, constant: -20)
                    ]
                } else {
                    constraints = [
                        toast.centerXAnchor.constraint(equalTo: window.centerXAnchor),
                        toast.leftAnchor.constraint(greaterThanOrEqualTo: window.leftAnchor, constant: 40),
                        toast.rightAnchor.constraint(lessThanOrEqualTo: window.rightAnchor, constant: -40),
                        toast.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: -20)
                    ]
                }
                NSLayoutConstraint.activate(constraints)
                UIView.animate(withDuration: 0.3, animations: {
                    toast.alpha = 1
                }) {
                    finished in
                    UIView.animate(withDuration: 0.3, delay: 3.5, options: [], animations: {
                        toast.alpha = 0
                    }) {
                        finished in
                        self.toast = nil
                        toast.removeFromSuperview()
                    }
                }
            }
            result(true)
        case "getMetaData":
            guard let key = call.arguments as? String else {
                result(nil)
                return
            }
            result(Bundle.main.infoDictionary?[key])
        case "showNotification":
            guard let arguments = call.arguments as? [String: Any],
                  let title = arguments["title"] as? String,
                  let text = arguments["text"] as? String,
                  let sound = arguments["sound"] as? Bool else {
                result(false)
                return
            }
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = text
            if sound {
                content.sound = UNNotificationSound.default
            }
            UNUserNotificationCenter.current().getNotificationSettings() {
                settings in
                if settings.authorizationStatus == .authorized {
                    UNUserNotificationCenter.current().add(UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil))
                    result(true)
                } else {
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
                        success, error in
                        if success {
                            UNUserNotificationCenter.current().add(UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil))
                            result(true)
                        } else {
                            result(false)
                        }
                    }
                }
            }
        case "scanQrCode":
            let controller = UIStoryboard(name: "Storyboard", bundle: Bundle(for: ScannerViewController.self)).instantiateInitialViewController() as! ScannerViewController
            controller.delegate = self
            pendingResult?(nil)
            pendingResult = result
            UIApplication.shared.keyWindow?.topViewController?.present(controller, animated: true)
        case "disableIdleTimer":
            UIApplication.shared.isIdleTimerDisabled = true
            result(true)
        case "enableIdleTimer":
            UIApplication.shared.isIdleTimerDisabled = false
            result(true)
        case "signInWithApple":
            guard #available(iOS 13.0, *),
                  let arguments = call.arguments as? [String: Any] else {
                result(nil)
                return
            }
            pendingResult?(nil)
            pendingResult = result
            let request = ASAuthorizationAppleIDProvider().createRequest()
            if let requestedScopes = arguments["requestedScopes"] as? [String] {
                request.requestedScopes = requestedScopes.map({ ASAuthorization.Scope($0) })
            }
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    @available(iOS 13.0, *)
    public func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let credential = authorization.credential as? ASAuthorizationAppleIDCredential else {
            pendingResult?(nil)
            pendingResult = nil
            return
        }
        var result = ["user": credential.user]
        if let authorizationCode = credential.authorizationCode {
            result["authorizationCode"] = String(data: authorizationCode, encoding: .utf8)
        }
        if let identityToken = credential.identityToken {
            result["identityToken"] = String(data: identityToken, encoding: .utf8)
        }
        if let email = credential.email {
            result["email"] = email
        }
        if let givenName = credential.fullName?.givenName {
            result["givenName"] = givenName
        }
        if let familyName = credential.fullName?.familyName {
            result["familyName"] = familyName
        }
        pendingResult?(result)
        pendingResult = nil
    }

    @available(iOS 13.0, *)
    public func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        if let error = error as? ASAuthorizationError, error.code != ASAuthorizationError.canceled {
            pendingResult?(nil)
            pendingResult = nil
        }
    }

    @available(iOS 13.0, *)
    public func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return UIApplication.shared.keyWindow!
    }

    func scannerDidScan(value: String) {
        pendingResult?(value)
        pendingResult = nil
    }

    func scannerDidCancel() {
        pendingResult?(nil)
        pendingResult = nil
    }
}
