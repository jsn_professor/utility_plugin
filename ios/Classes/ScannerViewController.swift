//
//  ScannerViewController.swift
//  Runner
//
//  Created by Professor on 2020/8/21.
//

import UIKit
import AVFoundation

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    private let devices = AVCaptureDevice.devices().filter({ $0.hasMediaType(.video) })
    private let metadataOutput = AVCaptureMetadataOutput()
    private let captureSession = AVCaptureSession()
    private var previewLayer: AVCaptureVideoPreviewLayer?
    private var currentDevice: AVCaptureDevice?
    weak var delegate: ScannerViewControllerDelegate?

    override var shouldAutorotate: Bool {
        return false
    }

    override func viewDidLoad() {
        currentDevice = devices.first
        configurateCaptureSession()
    }

    override func viewWillAppear(_ animated: Bool) {
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard let readableObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
              let stringValue = readableObject.stringValue,
              !stringValue.isEmpty else {
            return
        }
        dismiss(animated: true)
        delegate?.scannerDidScan(value: stringValue)
    }

    private func configurateCaptureSession() {
        guard let currentDevice = currentDevice,
              let videoInput = try? AVCaptureDeviceInput(device: currentDevice) else {
            dismiss(animated: true)
            delegate?.scannerDidCancel()
            return
        }
        captureSession.beginConfiguration()
        if !captureSession.inputs.isEmpty {
            for input in captureSession.inputs {
                captureSession.removeInput(input)
            }
        }
        captureSession.addInput(videoInput)
        if captureSession.outputs.isEmpty {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        }
        if self.previewLayer == nil {
            let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.frame = view.layer.bounds
            previewLayer.videoGravity = .resizeAspectFill
            if (previewLayer.connection?.isVideoOrientationSupported == true) {
                switch (UIApplication.shared.statusBarOrientation) {
                case .portrait:
                    previewLayer.connection?.videoOrientation = .portrait
                case .portraitUpsideDown:
                    previewLayer.connection?.videoOrientation = .portraitUpsideDown
                case .landscapeRight:
                    previewLayer.connection?.videoOrientation = .landscapeRight
                case .landscapeLeft:
                    previewLayer.connection?.videoOrientation = .landscapeLeft
                default:
                    break
                }
            }
            view.layer.insertSublayer(previewLayer, at: 0)
            self.previewLayer = previewLayer
        }
        captureSession.commitConfiguration()
    }

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
        delegate?.scannerDidCancel()
    }

    @IBAction func switchCamera(_ sender: Any) {
        guard let currentDevice = currentDevice,
              var index = devices.firstIndex(of: currentDevice) else {
            dismiss(animated: true)
            delegate?.scannerDidCancel()
            return
        }
        index += 1
        if index < devices.count {
            self.currentDevice = devices[index]
        } else {
            self.currentDevice = devices.first
        }
        configurateCaptureSession()
    }
}

protocol ScannerViewControllerDelegate: class {
    func scannerDidScan(value: String)
    func scannerDidCancel()
}
