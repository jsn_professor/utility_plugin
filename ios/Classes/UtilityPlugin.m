#import "UtilityPlugin.h"

#if __has_include(<utility_plugin/utility_plugin-Swift.h>)

#import <utility_plugin/utility_plugin-Swift.h>

#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "utility_plugin-Swift.h"

#endif

@implementation UtilityPlugin
+ (void)registerWithRegistrar:(NSObject <FlutterPluginRegistrar> *)registrar {
    [SwiftUtilityPlugin registerWithRegistrar:registrar];
}
@end
