// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authorization_apple_id_credential.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthorizationAppleIDCredential _$AuthorizationAppleIDCredentialFromJson(Map<String, dynamic> json) {
  return AuthorizationAppleIDCredential()
    ..authorizationCode = json['authorizationCode'] as String? ?? ''
    ..identityToken = json['identityToken'] as String? ?? ''
    ..user = json['user'] as String?
    ..email = json['email'] as String?
    ..givenName = json['givenName'] as String?
    ..familyName = json['familyName'] as String?;
}

Map<String, dynamic> _$AuthorizationAppleIDCredentialToJson(AuthorizationAppleIDCredential instance) => <String, dynamic>{
      'authorizationCode': instance.authorizationCode,
      'identityToken': instance.identityToken,
      'user': instance.user,
      'email': instance.email,
      'givenName': instance.givenName,
      'familyName': instance.familyName,
    };
