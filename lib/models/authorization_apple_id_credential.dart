import 'package:json_annotation/json_annotation.dart';

part 'authorization_apple_id_credential.g.dart';

@JsonSerializable()
class AuthorizationAppleIDCredential {
  @JsonKey(defaultValue: '', name: 'authorizationCode')
  late String authorizationCode;
  @JsonKey(defaultValue: '', name: 'identityToken')
  late String identityToken;
  @JsonKey(name: 'user')
  String? user;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'givenName')
  String? givenName;
  @JsonKey(name: 'familyName')
  String? familyName;

  AuthorizationAppleIDCredential();

  factory AuthorizationAppleIDCredential.fromJson(Map<String, dynamic> json) => _$AuthorizationAppleIDCredentialFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorizationAppleIDCredentialToJson(this);

  @override
  String toString() {
    return 'AuthorizationAppleIDCredential{user: $user, authorizationCode: $authorizationCode, identityToken: $identityToken, email: $email, givenName: $givenName, familyName: $familyName}';
  }
}
