import 'package:flutter/material.dart';
import 'package:utility_plugin/constant.dart';
import 'package:utility_plugin/models/authorization_apple_id_credential.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AppleSignIn extends StatefulWidget {
  final List<AuthorizationScope>? requestedScopes;
  final String? clientId;
  final Uri? redirectUri;
  final void Function(AuthorizationAppleIDCredential? credential)? onComplete;

  const AppleSignIn({Key? key, this.requestedScopes, this.clientId, this.redirectUri, this.onComplete}) : super(key: key);

  @override
  State<AppleSignIn> createState() => _AppleSignInState();
}

class _AppleSignInState extends State<AppleSignIn> {
  late WebViewController _controller;

  @override
  void initState() {
    super.initState();
    final queryParameters = {
      'client_id': widget.clientId,
      'redirect_uri': widget.redirectUri?.toString(),
      'response_type': 'code id_token',
    };
    final requestedScopes = widget.requestedScopes;
    if (requestedScopes != null && requestedScopes.isNotEmpty) {
      queryParameters['response_mode'] = 'form_post';
      queryParameters['scope'] = requestedScopes.map((e) {
        switch (e) {
          case AuthorizationScope.fullName:
            return 'name';
          case AuthorizationScope.email:
            return 'email';
        }
      }).join(' ');
    }
    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(NavigationDelegate(
        onNavigationRequest: (request) {
          final uri = Uri.parse(request.url);
          if (uri.authority == 'localhost' ||
              uri.authority == '127.0.0.1' ||
              uri.authority == widget.redirectUri?.authority && uri.path == widget.redirectUri?.path) {
            final code = uri.queryParameters['code'];
            final idToken = uri.queryParameters['id_token'];
            if (code != null && idToken != null) {
              widget.onComplete?.call(
                AuthorizationAppleIDCredential()
                  ..authorizationCode = code
                  ..identityToken = idToken,
              );
            } else {
              widget.onComplete?.call(null);
            }
            return NavigationDecision.prevent;
          } else {
            return NavigationDecision.navigate;
          }
        },
      ))
      ..loadRequest(Uri.https('appleid.apple.com', '/auth/authorize', queryParameters));
  }

  @override
  Widget build(BuildContext context) {
    WebViewController? webViewController;
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          final canGoBack = await webViewController?.canGoBack() ?? false;
          if (canGoBack) {
            webViewController?.goBack();
            return false;
          } else {
            return true;
          }
        },
        child: WebViewWidget(controller: _controller),
      ),
    );
  }
}
