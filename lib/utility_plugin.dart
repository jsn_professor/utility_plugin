import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:utility_plugin/constant.dart';
import 'package:utility_plugin/models/authorization_apple_id_credential.dart';
import 'package:utility_plugin/widgets/apple_sign_in.dart';

class UtilityPlugin {
  static const MethodChannel _methodChannel = MethodChannel('utility_plugin_method_channel');

  static Future<bool> downloadUpdate(String url, {String? title}) {
    return _methodChannel.invokeMethod('downloadUpdate', {
      'url': url,
      'title': title,
    }).then((value) => value ?? false);
  }

  static Future<bool> openBrowser(
    String url, {
    Color? color,
    bool? forceSafariVC,
    bool forceWebView = false,
    bool enableJavaScript = false,
    bool enableDomStorage = false,
    bool universalLinksOnly = false,
    Map<String, String> headers = const <String, String>{},
    Brightness? statusBarBrightness,
  }) async {
    if (Platform.isAndroid) {
      final Map<String, dynamic> arguments = {'url': url};
      if (color != null) {
        arguments['color'] = color.value;
      }
      final result = await _methodChannel.invokeMethod('openBrowser', arguments) ?? false;
      if (result) {
        return result;
      }
    }
    return launch(
      url,
      forceSafariVC: forceSafariVC,
      forceWebView: forceWebView,
      enableJavaScript: enableJavaScript,
      enableDomStorage: enableDomStorage,
      universalLinksOnly: universalLinksOnly,
      headers: headers,
      statusBarBrightness: statusBarBrightness,
    );
  }

  static Future<bool> openSettings() async {
    return await _methodChannel.invokeMethod('openSettings') ?? false;
  }

  static Future<bool> showToast(String text) async {
    return await _methodChannel.invokeMethod('showToast', text) ?? false;
  }

  static Future getMetaData(String key) async {
    return await _methodChannel.invokeMethod('getMetaData', key);
  }

  static Future<bool> showNotification(String title, String text, {bool sound = false, bool vibrate = false, String? notificationChannel}) async {
    return await _methodChannel.invokeMethod('showNotification', {
          'title': title,
          'text': text,
          'sound': sound,
          'vibrate': vibrate,
          'notificationChannel': notificationChannel,
        }) ??
        false;
  }

  static Future<String?> scanQrCode() {
    return _methodChannel.invokeMethod('scanQrCode');
  }

  static Future<bool> disableIdleTimer() async {
    return await _methodChannel.invokeMethod('disableIdleTimer') ?? false;
  }

  static Future<bool> enableIdleTimer() async {
    return await _methodChannel.invokeMethod('enableIdleTimer') ?? false;
  }

  static Future<AuthorizationAppleIDCredential?> signInWithApple(
    BuildContext context, {
    List<AuthorizationScope>? requestedScopes,
    String? clientId,
    Uri? redirectUri,
    Color? color,
  }) async {
    if (Platform.isAndroid) {
      final navigatorState = Navigator.maybeOf(context, rootNavigator: true);
      if (navigatorState != null) {
        return await navigatorState.push<AuthorizationAppleIDCredential?>(
          MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) => AppleSignIn(
              requestedScopes: requestedScopes,
              clientId: clientId,
              redirectUri: redirectUri,
              onComplete: (credential) {
                navigatorState.pop(credential);
              },
            ),
          ),
        );
      }
    }
    final arguments = <String, dynamic>{};
    final scopes = requestedScopes?.map((e) {
      switch (e) {
        case AuthorizationScope.fullName:
          return 'name';
        case AuthorizationScope.email:
          return 'email';
      }
    }).toList();
    if (scopes != null) {
      arguments['requestedScopes'] = scopes;
    }
    if (clientId != null) {
      arguments['clientId'] = clientId;
    }
    if (redirectUri != null) {
      arguments['redirectUri'] = redirectUri.toString();
    }
    if (color != null) {
      arguments['color'] = color.value;
    }
    final map = await UtilityPlugin._methodChannel.invokeMethod<Map>('signInWithApple', arguments);
    return map == null ? null : AuthorizationAppleIDCredential.fromJson(Map<String, dynamic>.from(map));
  }
}
