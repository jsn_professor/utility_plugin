import 'package:flutter/material.dart';
import 'package:utility_plugin/utility_plugin.dart';

void main() => runApp(App());

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Plugin example app'),
      ),
      body: ListView(
        children: ListTile.divideTiles(
          context: context,
          tiles: [
            ListTile(
              title: Text('Open settings'),
              onTap: () {
                UtilityPlugin.openSettings();
              },
            ),
            ListTile(
              title: Text('Open browser'),
              onTap: () {
                UtilityPlugin.openBrowser('https://www.google.com', color: Colors.orange);
              },
            ),
            ListTile(
              title: Text('Show toast'),
              onTap: () {
                UtilityPlugin.showToast(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
              },
            ),
            ListTile(
              title: Text('Get metadata'),
              onTap: () async {
                print(await UtilityPlugin.getMetaData('CFBundleName'));
              },
            ),
            ListTile(
              title: Text('Show notification'),
              onTap: () {
                UtilityPlugin.showNotification(
                  'Notification title',
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                );
              },
            ),
            ListTile(
              title: Text('Scan QR Code'),
              onTap: () async {
                var value = await UtilityPlugin.scanQrCode();
                print(value);
              },
            ),
            ListTile(
              title: Text('Sign in with Apple'),
              onTap: () async {
                final credential = await UtilityPlugin.signInWithApple(
                  context,
                  clientId: 'com.sctw.utilityplugin.service',
                  redirectUri: Uri.parse('https://utilityplugin.page.link/uy1b'),
                );
                print(credential);
              },
            ),
            ListTile(
              title: Text('Disable Idle Timer'),
              onTap: () {
                UtilityPlugin.disableIdleTimer();
              },
            ),
            ListTile(
              title: Text('Download App Update'),
              onTap: () async {
                final result = await UtilityPlugin.downloadUpdate('https://api.onedrive.com/v1.0/shares/');
                UtilityPlugin.showToast('$result');
              },
            ),
          ],
        ).toList(),
      ),
    );
  }
}
